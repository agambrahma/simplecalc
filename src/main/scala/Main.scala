import Calc._

import scala.io.StdIn.readLine

object Main extends App {
  println("Simple calculator\n\n")

  var done = false
  while (!done) {
    print("Enter an expression, or 'END' to quit: ")
    val input = readLine()

    if (input == "END") {
      done = true
    } else {
      println(s"Result: ${Calc.process(input)} \n")
    }
  }
}
