import fastparse._
import NoWhitespace._

object Calc {
  // Note: for simplicity (avoiding numeric promotion etc), just have "one number type".

  def numParser[_: P] = P( ("-".? ~ CharIn("0-9").rep ~ ".".? ~ CharIn("0-9").rep).!.map(_.toDouble) )
  def parenParser[_: P]: P[Double] = P( "(" ~/ exprParser ~ ")"  )  // needs explicit type because it's recursive.
  def factorParser[_: P] = P( parenParser | numParser )
  def termParser[_: P] = P( factorParser ~ (CharIn("*/").! ~/ factorParser).rep ).map(eval)
  def exprParser[_: P] = P( termParser ~ (CharIn("+\\-").! ~/ termParser).rep ).map(eval)

  // The top-level "string-y" input/output.
  def process(line: String): String  = {
    if (line.isEmpty) return ""
    val filteredLine = line.filter( (char) => char != ' ' )

    try {
      parse(filteredLine, exprParser(_)) match {
        case Parsed.Success(result, _) => s"$result"
        case Parsed.Failure(error) => s"Calc Input Error: $error"
      }
    } catch {
      case e: java.lang.NumberFormatException => s"Calc Input Error: malformed input [$line]"
    }
  }

  def eval(ast: (Double, Seq[(String, Double)])): Double = {
    val (base, ops) = ast
    ops.foldLeft(base) {
      case (n1, (op, n2)) =>
        //println(s"Debug: n1=> $n1, n2=> $n2")
        op match {
        case "*" => n1 * n2
        case "/" => n1 / n2
        case "+" => n1 + n2
        case "-" => n1 - n2
      }
    }
  }
}
