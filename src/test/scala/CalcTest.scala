import Calc._
import org.scalatest.funsuite.AnyFunSuite

class CalcTest extends AnyFunSuite {
  test("no-op") {
    assert(Calc.process("") == "")
  }

  test("numbers") {
    val input = List("64", "-43", "21", "10049.234", "185", "-679.28")
    val output = List("64.0", "-43.0", "21.0", "10049.234", "185.0", "-679.28")
    for ((i,o) <- input.zip(output)) {
      assert(Calc.process(i) == o)
    }
  }

  test("invalid") {
    val inputs = List("4+", "foo")
    for (input <- inputs) {
      assert(Calc.process(input) == s"Calc Input Error: malformed input [$input]")
    }
  }

  test("parens") {
    val input = List("(12)", "4", "(-56.2)", "(10000.00)")
    val output = List("12.0", "4.0", "-56.2", "10000.0")
    for ((i,o) <- input.zip(output)) {
      assert(Calc.process(i) == o)
    }
  }

  test("terms") {
    val inputoutput = List(
      ("4*5", "20.0"),
      ("8/2", "4.0"),
    )
    for ((i,o) <- inputoutput) {
      assert(Calc.process(i) == o)
    }
  }

  test("expressions") {
    val inputoutput = List(
      ("2+3", "5.0"),
      ("1+1*2", "3.0"),
      ("1+(1*2)", "3.0"),

      // Whitespace
      (" 1 + 2 ", "3.0"),
      (" 5 + 6 * 7.2 ", "48.2"),
    )
    for ((i,o) <- inputoutput) {
      assert(Calc.process(i) == o)
    }
  }
}

